# secpass script by NillyTheL0L
import random


# Variables
lower_chars = "abcdefghijklmnopqrstuvwxyz"
upper_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
nums = "0123456789"

# Mounting
all = lower_chars + upper_chars + nums
size = 14
pwd = "".join(random.sample(all,size))

# Printing
print("Password generated:" + pwd)